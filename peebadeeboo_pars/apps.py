from django.apps import AppConfig


class PeebadeebooParsConfig(AppConfig):
    name = 'peebadeeboo_pars'
