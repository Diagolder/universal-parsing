from django.shortcuts import render, HttpResponseRedirect
from .Parsing.main import  start, new_site_template
import json
# Create your views here.


def template(request):
    if request.method == 'POST':
        file = request.FILES['file']
        file_name = str(file)
        handle_uploaded_file(file)
        with open('file.txt', 'r') as f:
            data = f.read()

        is_template_exists = False

        if request.POST.get('time'):
            is_template_exists = new_site_template(data, file_name, request.POST['time'], request.POST['every'], is_delete=False, old_template=False)

        saved_data = start(data)

        if type(saved_data) is type("123"):
            request.session['saved_data'] = saved_data
            return HttpResponseRedirect('/result/')
            
        if saved_data:
            saved_data = json.dumps(saved_data, ensure_ascii=False)
            request.session['saved_data'] = saved_data
        else:
            request.session['saved_data'] = 'Successfully done'

        if is_template_exists:
            request.session['data'] = data
            request.session['file_name'] = file_name
            request.session['time'] = request.POST['time']
            request.session['every'] = request.POST['every']
            return HttpResponseRedirect('/template_already_exists_in_crontab/')

        return HttpResponseRedirect('/result/')

    return render(request, 'peebadeeboo_pars/template.html')


def result(request):
    saved_data = request.session['saved_data']
    del request.session['saved_data']

    return render(request, 'peebadeeboo_pars/result.html', context={'saved_data': saved_data})


def template_already_exists_in_crontab(request):
    if request.method == 'POST':
        if request.POST.get('Delete') == 'on':
            new_site_template(request.session['data'], request.session['file_name'], request.session['time'], request.session['every'], is_Delete=True, old_template=True)




        del request.session['data'], request.session['file_name'], request.session['time'], request.session['every']
        return HttpResponseRedirect('/result/')
    else:
        return render(request, 'peebadeeboo_pars/template_already_exists_in_crontab.html')

def handle_uploaded_file(f):
    with open('file.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
