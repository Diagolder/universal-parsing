try:
    from .components.todo import Todo, user_saved_data
    from .components.parsing_tasks import ParsingTasks
    from .components.db import DB
except:
    from components.todo import Todo, user_saved_data
    from components.parsing_tasks import ParsingTasks
    from components.db import DB
import datetime
import bs4
import sys
import pathlib
import requests
from crontab import CronTab

need_save_to_db = {}
new_articles = 0 # Для сообщения в телеграм, подсчитывает кол-во новых статей
now = datetime.datetime.now() # Для сообщения в телеграм


def save_bd():
    global new_articles

    db = DB()
    db.connect()

    save_here = [] # Сохранить в эти поля базы данных
    save_this = [] # Сохранить эту информацию

    # need_save_to_db выглядит таким образом:
    # {'куда_сохранить': 'что_сохранить'...}
    # key = куда_сохранить  value = что_сохранить
    for key, value in need_save_to_db.items():
        save_here.append(key)

        # Если в поле БД нужно записать две переменные, выполняется это условие. 
        # К примеру, В поле caption записывается Header (название статьи) и Content (текст статьи)
        if type(value) is list:
            # Временная переменная для save_this
            temp = []
            for i in range(len(value)):
                value[i] = user_saved_data[value[i]]
            # Так как данные из этих двух переменных я должен соединить в одно, 
            # я вытаскиваю каждый элемент массива, и склеиваю их в переменную tmp
            for i in range(len(value[0])):
                tmp = []
                for j in range(len(value)):
                    tmp.append(value[j][i])
                # Бывают случаи, когда кроме Header и Content есть еще Description, 
                # но он встречается не в каждой статье, поэтому тут идет проверка
                for x in tmp:
                    if not x:
                        tmp.remove(x)

                temp.append('\n'.join(tmp))
            save_this.append(temp)
        else:
            # Иногда в поле БД нужно сохранить не переменную, а число. 
            # К примеру, поле source_id, требующая константную цифру
            # И так как это цифра, в списке сохраненных переменных её не будет
            if user_saved_data.get(value) is None:
                save_this.append(value)
            else:
                save_this.append(user_saved_data[value])

    # Повторно вытаскиваю object_id в другую переменную, чтобы проверить на дубликаты
    object_id = user_saved_data[need_save_to_db['object_id']]
    
    # Проблема сохранении переменных, пока не разобрался
    if len(object_id) == 1:
        object_id = object_id[0]

    # Создаю запрос в базу данных
    query = 'insert into tl_media_data_news (' + ', '.join(save_here) + ') values (' + \
            (('%s, ' * len(save_here))[:-2]) + ')'
    
    # Теперь я собираю каждый элемент каждого массива в одно, чтобы все отправить в БД
    i = 0
    while i in range(len(save_this[0])):
        # Временная переменная для value
        tmp = []
        j = 0
        while j in range(len(save_this)):
            # На случай, если я наткнулся на source_id (константную цифру)
            if type(save_this[j]) is not list:
                tmp.append(save_this[j])
            else:
                tmp.append(save_this[j][i])
            j += 1
        # Переменная value - все нужные данные, которые идут в БД вместе с запросом (query)
        value = tmp

        if type(value[0]) is list:
            continue
        
        duplicate_query = "SELECT * FROM tl_media_data_news WHERE object_id = '" + object_id[i] + "'"
        
        if not db.get(duplicate_query):
            new_articles += 1
            db.set(query, value)
        i += 1

def start(tasks_txt, need_show_saved_data=True):
    global need_save_to_db

    tasks_txt = tasks_txt.split('\n')
    tasks_txt = list(filter(('').__ne__, tasks_txt)) # Убираю все пустые строки в массиве
    
    pars_tasks = ParsingTasks(tasks_txt)
    pars_tasks.start()
    need_save_to_db = pars_tasks.db.copy()
    
    todo = Todo(pars_tasks.task_list, {})
    error_message = todo.start()
    
    if error_message:
        return error_message
        
    if need_save_to_db:
        save_bd()
        
    for k, v in user_saved_data.items():
        i = 0
        tmp = bs4.BeautifulSoup('<img>', 'html.parser')
        tmp = tmp.find('img')
        while i in range(len(v)):
            if type(v[i]) == type(tmp):
                v[i] = v[i].__str__()
            if type(v[i]) is datetime.datetime:
                v[i] = v[i].strftime('%Y-%m-%d %H:%M:%S')
            i += 1

    if user_saved_data and need_show_saved_data:
        return user_saved_data

def main():
    try:
        file_name = sys.argv[1]

        text = 'Source is Exclusive \nTime is ' + str(now) + '\n' + file_name + ' started!'
        telegram_bot_sendtext(text)

        db = DB()
        db.connect()
        file_data = db.get('SELECT data FROM templates WHERE name = \'' + file_name + '\'')[0]
        
        start(file_data, need_show_saved_data=False)

        text = file_name + ' finish!' + '\n' + 'New Article count: ' + str(new_articles)
        telegram_bot_sendtext(text)
    except Exception as e:
        text = file_name + ' Error!' + '\n' + str(e)
        telegram_bot_sendtext(text)

def telegram_bot_sendtext(bot_message):
    bot_token = '558839765:AAEbBj0Jl9WqytQptSGodcx3z-x8YSx2Lwc'
    bot_chatID = '489213451'
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

def new_site_template(data, file_name, time, every, is_delete, old_template):

    virtualenv_path = 'Desktop/universal-parsing/venv/bin/python'
    script_path = 'Desktop/universal-parsing/peebadeeboo_pars/Parsing/main.py'
    
    command = virtualenv_path + ' ' + script_path + ' ' + file_name

    db = DB()
    db.connect()

    cron = CronTab(user=True)
    # there i check, if this job already exists, i exit from here
    for job in cron.find_command(command):
        if job:
            # if is_delete True, that's mean, that user accept delete old template
            if is_delete:
                cron.remove(job)
                break
            return True

    job = cron.new(command)

    put_job_in_right_time(job, time, every)

    cron.write()

    query = "Select * from templates where name='{}'".format(file_name)
    
    if db.get(query) or old_template:
        db.execute("Delete from templates where name = '" + file_name + "'")

    db.set("insert into templates (name, data) values (%s, %s)", [file_name, data])
    return False

def put_job_in_right_time(job, time, every):
    if every == 'Minute':
        job.minute.every(time)
    elif every == 'Hour':
        job.hour.every(time)
    elif every == 'Day':
        job.day.every(time)

if __name__ == '__main__':
    main()