import requests
import re
import datetime
from bs4 import BeautifulSoup as Bs
from .task import Task as TaskClass
from .alert import *
from .date import get_date


class Command:
    task = None  # Task type variable

    def __init__(self, todo, task, user_saved_data):
        self.taken_data = todo.taken_data
        self.user_saved_data = user_saved_data
        self.task = task
        self.task_count = todo.task_count  #need only for alerts

    def run_command(self):
        command = self.task.command_name

        # Вызываю метод класса Command, передавая название метода
        method_to_call = getattr(Command, command) 
        status = method_to_call(self)

        if status:
            return status

    def url(self):
        url = self.task.name.split()[1:]

        if len(url) != 1:
            for key, value in self.taken_data.items():
                if url[0] == key:
                    url[0] = value
                elif url[2] == key:
                    url[2] = value
            url = url[0] + url[2]
        else:
            for key, value in self.taken_data.items():
                if url[0] == key:
                    url[0] = value
            url = url[0]

        response = requests.get(url)
        soup = Bs(response.text, 'html.parser')

        TaskClass.current_url = url
        TaskClass.current_html = soup

    def print(self):
        need_print = self.task.name.split()[1]

        try:
            print(self.taken_data[need_print])
            return
        except KeyError:
            pass

        print(need_print)

    def save(self):
        status = ''

        if self.task.name.find('\s') != -1:
            self.task.name = self.task.name.replace('\s', '')
            status = 'skip'
        elif self.task.name.find('\c') != -1:
            self.task.name = self.task.name.replace('\c', '')
            status = 'continue'

        need_save = re.sub(',', ' ', self.task.name).split()[1:]
        for i in need_save:
            if not self.taken_data.get(i):
                self.taken_data[i] = None
            try:
                self.user_saved_data[i]
            except KeyError:
                if type(self.taken_data.get(i)) is list:
                    self.user_saved_data[i] = self.taken_data.get(i)
                else:
                    self.user_saved_data[i] = [self.taken_data.get(i)]
            else:
                if type(self.user_saved_data[i]) is not list:
                    tmp = self.user_saved_data[i]
                    self.user_saved_data[i] = [tmp, self.taken_data[i]]
                else:
                    self.user_saved_data[i].append(self.taken_data[i])


    def variable(self):
        status = ''

        if self.task.name.find('\s') != -1:
            self.task.name = self.task.name.replace('\s', '')
            status = 'skip'
        elif self.task.name.find('\c') != -1:
            self.task.name = self.task.name.replace('\c', '')
            status = 'continue'

        # in this variable need save data
        save_here = self.task.name.split()[0]
        
        find_this = re.search(r'<\w+ \w+( *)=( *)"[^>]+">', self.task.name).group(0)

        find_this = processing_tag(find_this)

        # if in task.name we find '[]', this variable want to be array
        find_many = self.task.name.find('[]') != -1

        found_data = find_tag(find_this.split('!@'), self.task.current_html, find_many)

        # if found_data will be array, and without data, 'not found_data' give true
        if not found_data:
            if status:
                return status
            error_data = {'In Line': self.task_count, 'Task': self.task.name,
                          'Error': 'Can\'t find tag'}
            return error(error_data)

        self.taken_data[save_here] = found_data

    def find(self):

        status = ''

        if self.task.name.find('\s') != -1:
            self.task.name = self.task.name.replace('\s', '')
            status = 'skip'
        if self.task.name.find('\c') != -1:
            self.task.name = self.task.name.replace('\c', '')
            status = 'continue'

        self.task.name = re.sub('.find', ' ', self.task.name).split()

        save_here = self.task.name[0]

        is_array = False

        if self.task.name[1] == '[]':
            is_array = True

        search_here = self.task.name[2]

        if is_array:
            search_here = self.task.name[3]

        if is_array:
            find_this = self.task.name[4:]
        else:
            find_this = self.task.name[3:]

        find_this = processing_tag(' '.join(find_this))

        if not self.taken_data.get(search_here):
            if status:
                return status
            error_data = {'In Line': self.task_count, 'Task': self.task.name,
                          'Error': 'Variable, where i need find tag empty'}
            return error(error_data)

        found_tag = find_tag(find_this.split('!@'), self.taken_data.get(search_here), is_array)

        if not found_tag:
            if status:
                return status
            error_data = {'In Line': self.task_count, 'Task': self.task.name,
                          'Error': 'Can\'t find tag'}
            return error(error_data)

        self.taken_data[save_here] = found_tag

    def geturl(self):
        task_name = self.task.name

        status = ''

        if task_name.find('\s') != -1:
            task_name = task_name.replace('\s', '')
            status = 'skip'
        elif task_name.find('\c') != -1:
            task_name = task_name.replace('\c', '')
            status = 'continue'

        task_name = re.sub('.getURL', ' ', self.task.name).split()

        save_here = task_name[0]

        search_here = task_name[2]

        domain = ''

        try:
            domain = task_name[3]
            domain = domain.replace('(', '')
            domain = domain.replace(')', '')
        except IndexError:
            pass

        search_here = self.taken_data.get(search_here)

        if not search_here:
            if status:
                return status
            error_data = {'In Line': str(self.task_count), 'Task': self.task.name,
                          'Error': 'Variable, where i need find url was empty'}
            return error(error_data)

        is_array = True

        try:
            search_here[0]
        except KeyError:
            is_array = False

        tmp = []

        if is_array:
            for i in search_here:
                try:
                    url = i.find('a')['href']
                    if domain:
                        tmp.append(domain + url)
                    else:
                        tmp.append(url)
                except KeyError:
                    if status == 'continue':
                        continue
                    elif status == 'skip':
                        return status
                    else:
                        error_data = {'In Line': str(self.task_count), 'Task': self.task.name,
                                      'Error': 'Can\'t find href attribute'}
                        return error(error_data)
                except TypeError:
                    if status == 'continue':
                        continue
                    elif status == 'skip':
                        return status
                    else:
                        error_data = {'In Line': str(self.task_count), 'Task': self.task.name,
                                      'Error': 'In this element i don\'t find <a> tag'}
                        return error(error_data)
                # if domain empty, 'if' doesn't work

        else:
            try:
                url = search_here.find('a')['href']
                if domain:
                    tmp = domain + url
                else:
                    tmp = url
            except KeyError:
                if status:
                    return status
                error_data = {'In Line': str(self.task_count), 'Task': self.task.name,
                              'Error': 'Can\'t find href attribute'}
                return error(error_data)
            except TypeError:
                if status:
                    return status
                error_data = {'In Line': str(self.task_count), 'Task': self.task.name,
                              'Error': 'In this element i don\'t find <a> tag'}
                return error(error_data)

        self.taken_data[save_here] = tmp

    def text(self):
        original_task_name = self.task.name
        status = ''

        if self.task.name.find('\s') != -1:
            self.task.name = self.task.name.replace('\s', '')
            status = 'skip'
        elif self.task.name.find('\c') != -1:
            self.task.name = self.task.name.replace('\c', '')
            status = 'continue'

        self.task.name = re.sub('.text', '', self.task.name).split()

        save_here = self.task.name[0]

        text_this = self.task.name[2]

        is_array = True

        try:
            self.taken_data[text_this][0]
        except KeyError:
            is_array = False

        if is_array:
            tmp = []
            if not self.taken_data.get(text_this):
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, which i need translate to text empty'}
                return error(error_data)
            for i in self.taken_data[text_this]:
                try:
                    tmp.append(i.text)
                except AttributeError:
                    if status:
                        return status
                    error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, which i need translate to text can\'t be translate'}
                    return error(error_data)
            tmp = '\n'.join(tmp)
        else:
            if not self.taken_data.get(text_this):
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, which i need translate to text empty'}
                return error(error_data)
            try:
                tmp = self.taken_data[text_this].text
            except AttributeError:
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, which i need translate to text can\'t be translate'}
                return error(error_data)

        self.taken_data[save_here] = tmp

    def getattribute(self):
        original_task_name = self.task.name
        status = ''

        if self.task.name.find('\s') != -1:
            self.task.name = self.task.name.replace('\s', '')
            status = 'skip'
        elif self.task.name.find('\c') != -1:
            self.task.name = self.task.name.replace('\c', '')
            status = 'continue'

        self.task.name = re.sub('.getAttribute', ' ', self.task.name).split()

        save_here = self.task.name[0]

        search_here = self.task.name[2]

        is_array = True

        try:
            self.taken_data[search_here][0]
        except KeyError:
            is_array = False

        attr = self.task.name[3]
        attr = attr.replace('(', '')
        attr = attr.replace(')', '')

        if is_array:
            tmp = []
            if not self.taken_data.get(search_here):
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, where i need find attribute was empty'}
                return error(error_data)
            for i in self.taken_data[search_here]:
                # i do this, because sometimes in i[attr] can be this ['asd', 'asd', 'asd']. asd - attribute
                try:
                    if type(i[attr]) is list:
                        tmp.append(' '.join(i[attr]))
                    else:
                        tmp.append(i[attr])
                except KeyError:
                    if status == 'skip':
                        return status
                    elif status == 'continue':
                        continue
                    error_data = {'In Line': self.task_count, 'Task': original_task_name,
                                  'Error': 'Variable, where i need find attribute hasn\'t this attribute'}
                    return error(error_data)
        else:
            if not self.taken_data.get(search_here):
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, where i need find attribute was empty'}
                return error(error_data)
            try:
                tmp = self.taken_data[search_here][attr]
                if type(tmp) is list:
                    tmp = ' '.join(tmp)
            except KeyError:
                if status:
                    return status
                error_data = {'In Line': self.task_count, 'Task': original_task_name,
                              'Error': 'Variable, where i need find attribute hasn\'t this attribute'}
                return error(error_data)

        self.taken_data[save_here] = tmp

    def dodate(self):
        save_here = self.task.name.split()[0]

        self.task.name = re.sub('.dodate', '', self.task.name)

        date = self.taken_data[self.task.name.split()[2]]

        self.taken_data[save_here] = get_date(date)

    def get(self):
        save_here = self.task.name.split()[0]

        self.task.name = self.task.name.replace('.get(', ' ')[:-1]

        index = int(self.task.name.split()[3])

        saved_data = self.task.name.split()[2]
        saved_data = self.taken_data[saved_data][index]

        self.taken_data[save_here] = saved_data

    def cut(self):
        save_here = self.task.name.split()[0]

        self.task.name = self.task.name.replace('.cut(', ' ')[:-1]
        need_cut = self.task.name.split()[3]

        cut_there = self.task.name.split()[2]
        cut_there = self.taken_data[cut_there]

        self.taken_data[save_here] = cut_there.replace(need_cut, '')

    def concatenate(self): # only for text
        begin = self.task.name.find('~') != -1

        self.task.name = self.task.name.replace('~', '')

        save_here = self.task.name.split()[0]

        self.task.name = self.task.name.replace('.concatenate(', ' ')[:-1]
        concatenate_with = self.task.name.split()[2]
        concatenate_with = self.taken_data[concatenate_with]

        is_array = False
        if type(concatenate_with) is list:
            is_array = True

        need_concatenate = self.task.name.split()[3]

        if is_array:
            tmp = []
            for i in concatenate_with:
                if begin:
                    tmp.append(need_concatenate + i)
                else:
                    tmp.append(i + need_concatenate)
        else:
            if begin:
                tmp = need_concatenate + concatenate_with
            else:
                tmp = concatenate_with + need_concatenate

        self.taken_data[save_here] = tmp


def find_tag(split_tag_data, search_here, find_many):

    if (search_here is None) or not search_here:
        if len(split_tag_data) != 1:
            tag = '<' + split_tag_data[0] + ' ' + split_tag_data[1] + '="' + split_tag_data[2] + '">'
        else:
            tag = '<' + split_tag_data[0] + '>'
        error_data = {'Error': 'Try find this ' + tag + ', but didn\'t found, where to me search'}
        return error(error_data)

    tag = [split_tag_data[0]]

    if len(split_tag_data) != 1:
        tag.append(split_tag_data[1])
        tag.append(split_tag_data[2])

        if find_many:
            return search_here.find_all(tag[0], {tag[1]: tag[2]})
        else:
            return search_here.find(tag[0], {tag[1]: tag[2]})

    if find_many:
        return search_here.find_all(tag[0])
    else:
        return search_here.find(tag[0])


def processing_tag(tag):
    tag = re.sub('<', '', tag)
    tag = re.sub('=', ' ', tag)
    tag = re.sub('"', '', tag)
    tag = re.sub('>', '', tag)

    tag = tag.split()
    if len(tag) != 1:
        tag = tag[0] + '!@' + tag[1] + '!@' + ' '.join(tag[2:])
    else:
        tag = ' '.join(tag)

    return tag
