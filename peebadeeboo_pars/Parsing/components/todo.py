from .task import Task as Task_Class
from .command import Command
from .alert import error

user_saved_data = {}

class Todo:

    def __init__(self, missions, taken_data, task_count=1):
        self.tasks = []  # 
        self.taken_data = taken_data
        self.task_count = task_count

        for mission in missions:
            mission = Task_Class(mission)
            self.tasks.append(mission)

    def start(self):
        for Task in self.tasks:

            error_message = self.identify(Task)
            if error_message:
                return error_message

            if Task.__str__() == 'LIST':
                continue

            task_command = Command(self, Task, user_saved_data)
            status = task_command.run_command()
            self.task_count += 1

            if status == 'skip':
                break
            elif status == 'continue':
                pass
            elif status: # In this case will return error message
                return status

    def identify(self, task):
        task.command_name = None

        if type(task.name) is list:
            for i in self.taken_data[task.name[0]]:
                # print(task.name[1:])
                new_todo = Todo(task.name[1:], {'i': i})
                new_todo.task_count += self.task_count
                error_message = new_todo.start()
                
                if error_message:
                    return error_message
            return

        task.name = task.name.split()

        if task.name[0] == 'URL:':
            task.command_name = 'url'
        elif task.name[0] == 'Print:':
            task.command_name = 'print'
        elif task.name[0] == 'Save:':
            task.command_name = 'save'

        if task.command_name is not None:
            task.name = ' '.join(task.name)
            return

        task.name = ' '.join(task.name)

        if task.name.find('.find') is not -1:
            task.command_name = 'find'

        elif task.name.find('.getURL') is not -1:
            task.command_name = 'geturl'

        elif task.name.find('.text') is not -1:
            task.command_name = 'text'

        elif task.name.find('.getAttribute') is not -1:
            task.command_name = 'getattribute'
        elif task.name.find('dodate') is not -1:
            task.command_name = 'dodate'
        elif task.name.find('.get') is not -1:
            task.command_name = 'get'
        elif task.name.find('.cut') is not -1:
            task.command_name = 'cut'
        elif task.name.find('.concatenate') is not -1:
            task.command_name = 'concatenate'

        # if command does found, exit of function
        if task.command_name is not None:
            return


        if task.name.find('=') is not -1:
            task.command_name = 'variable'
        else:
            error_data = {'In Line': str(self.task_count), 'Task': task.name, 'Error': 'Can\'t find command'}
            return error(error_data)