import psycopg2


class DB:

    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self):
        try:
            self.connection = psycopg2.connect(user='postgres', password='1', host='127.0.0.1', port='5432', database='test1')

            self.cursor = self.connection.cursor()

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)

    def set(self, query, value):
        i = 0
        while i in range(len(value)):
            try:
                if len(value[i]) > 10000:
                    value[i] = value[i][:10000]
            except TypeError:
                pass
            i += 1

        self.cursor.execute(query, value)
        self.connection.commit()

    def get(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchone()

    def execute(self, query):
        self.cursor.execute(query)
        self.connection.commit()
