def error(error_data):
    error_data = treatment_data(error_data)
    error_message = 'Error occurred\n'
    for key, value in error_data.items():
        error_message += key + ': ' + value + '\n'
    return error_message


def warning(warning_data):
    warning_data = treatment_data(warning_data)
    warning_message = 'Warning!\n'
    for key, value in warning_data.items():
        warning_message += key + ': ' + value + '\n'
    print(warning_message)



def treatment_data(data):
    for key, value in data.items():
        if type(value) is list:
            data[key] = ' '.join(value)
        if type(value) is int:
            data[key] = str(value)

    return data
