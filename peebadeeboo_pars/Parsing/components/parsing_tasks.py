class ParsingTasks():

	def __init__(self, tasks):
		self.tasks = tasks  # Массив не обработанных строк из файла
		self.db = {} # Данные, которые нужно сохранить в базу данных 
		self.task_list = None # Упорядоченный список всех инструкций (task-ов)

	def start(self):
		tmp = []
		i = 0

		while i in range(len(self.tasks)):

			if self.tasks[i] == '[':
				old_tasks = self.tasks # ---------|
				self.tasks = self.tasks[i+1:] #---|--- Так как функция работает рекурсивно, нам нужно поменять данные в self.tasks. Чтобы не потерять старые данные я использую old_tasks
				result = self.start()
				self.tasks = old_tasks #----------|
				tmp.append(result[0])
				i += result[1] + 1

			elif self.tasks[i] == ']':
				return tmp, i

			elif self.tasks[i] == '{':
				i += 1
				while self.tasks[i] != '}':
					if len(self.tasks[i].split()[2:]) == 1:
						# Сохраняю в виде ключа и значения
						self.db[self.tasks[i].split()[0]] = self.tasks[i].split()[2] 
					else:
						# В этом случае в это в поле БД нужно сохранить не одну переменную, а две. Пример:
						# caption = Header Content
						self.db[self.tasks[i].split()[0]] = self.tasks[i].split()[2:]
					i += 1

			else:
				tmp.append(self.tasks[i])

			i += 1

		self.task_list = tmp

	def get_db():
		return self.db