from datetime import datetime, timedelta
import re

months = [
    "[Яя]нвар[ья]",
    "[Фф]еврал[ья]",
    "[Мм]арт[а]*",
    "[Аа]прел[ья]",
    "[Мм]а[йя]",
    "[Ии]юн[ья]",
    "[Ии]юл[ья]",
    "[Аа]вгуст[а]*",
    "[Сс]ентябр[ья]",
    "[Оо]ктябр[ья]",
    "[Нн]оябр[ья]",
    "[Дд]екабр[ья]",

    "[Ққ]аңтар",
    "[Аа]қпан",
    "[Нн]аурыз",
    "[Сс]әуір",
    "[Мм]амыр",
    "[Мм]аусым",
    "[Шш]ілде",
    "[Тт]амыз",
    "[Ққ]ыркүйек",
    "[Ққ]азан",
    "[Ққ]араша",
    "[Жж]елтоксан"
]

months_regexp = "|".join(["(" + month + "[,]{0,1})" for month in months])


def get_date(s):

    if s == 'Вчера':
        yesterday = datetime.now() - timedelta(days=1)
        return datetime(yesterday.year, yesterday.month, yesterday.day, 0, 0, 0)

    if re.search('\d\d:\d\d', s):
        time = re.search('\d\d:\d\d', s).group().split(':')
        time = [int(time[0]), int(time[1])]
    elif re.search('\d:\d\d', s):
        time = re.search('\d:\d\d', s).group().split(':')
        time = [int(time[0]), int(time[1])]
    else:
        time = [0, 0]

    if re.search("[Сс]егодня|[Бб]үгін", s):
        return datetime(datetime.now().year, datetime.now().month, datetime.now().day, time[0], time[1])
    elif re.search("\d\d[/.-]\d\d[/.-]\d\d\d\d", s):
        d = re.search("\d\d[/.-]\d\d[/.-]\d\d\d\d", s)
        d = re.split("[/.-]", d.group())

        return datetime(int(d[2]), int(d[1]), int(d[0]), time[0], time[1])
    elif re.search("\d\d\d\d[/.-]\d\d[/.-]\d\d", s):
        d=re.search("\d\d\d\d[/.-]\d\d[/.-]\d\d", s)
        d = re.split("[/.-]", d.group())
        return datetime(int(d[0]), int(d[1]), int(d[2]), time[0], time[1])
    elif re.search(months_regexp, s):
        y = re.search("\d\d\d\d", s)
        if re.search("\d\d\d\d", s):
            y = int(y.group())

        s = s.split(" ")

        for i, month in enumerate(months):
            for w in s:
                if re.search(month, w):
                    m = (i % 12) + 1

        for index, w in enumerate(s):
            if re.search(months_regexp, w):
                i = index

        s = s[i-1] if i > 0 else s[i+1]
        d = int(re.match("\d{1,2}", s).group())

        if not y:
            cy = datetime.now().year
            cm = datetime.now().month
            cd = datetime.now().day

            if m > cm:
                y = cy - 1
            elif m == cm:
                if d > cd:
                    y = cy - 1
                else:
                    y = cy
            else:
                y = cy

        return datetime(int(y), int(m), int(d), time[0], time[1])
    else:
        return datetime(datetime.now().year, datetime.now().month, datetime.now().day, time[0], time[1])
