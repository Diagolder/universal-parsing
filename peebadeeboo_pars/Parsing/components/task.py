class Task:
    current_url = None
    current_html = None

    def __init__(self, mission):
        self.name = mission
        self.command_name = None

    def __str__(self):
        if type(self.name) == list:
            return 'LIST'
        return self.name
