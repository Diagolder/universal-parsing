from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('template/', template),
    path('result/', result),
    path('template_already_exists_in_crontab/', template_already_exists_in_crontab),
]
